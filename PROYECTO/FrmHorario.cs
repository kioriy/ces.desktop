﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PROYECTO
{
    public partial class FrmHorario : Form
    {
        public FrmHorario()
        {
            InitializeComponent();
        }
        Alumno alumno_seleccionado = new Alumno();
        public string hora_entrada="";
        public string hora_salida = "";
        int entra = 0;
        private void Horario_Load(object sender, EventArgs e)
        {
            Alumno todos_los_alumnos = new Alumno();
            todos_los_alumnos.llenarComboBox(cbAlumno, todos_los_alumnos.queryConsulta(todos_los_alumnos.nombre_alumno_a, todos_los_alumnos.ALUMNO, ""), todos_los_alumnos.nombre_alumno_a, todos_los_alumnos.ALUMNO , ref entra );
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            hora_entrada = txtHoraEntrada.Text + ":" + txtMinutoEntrada.Text;
            hora_salida = txtHoraSalida.Text + ":" + txtMinutoSalida.Text;
            Horarios nuevo_horario = new Horarios(alumno_seleccionado.Id_alumno,this);
            nuevo_horario.insertarHorario();
        }
        private void cbAlumno_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbAlumno.SelectedIndex != -1)
            {
                try
                {
                    alumno_seleccionado.obtenerDatos = alumno_seleccionado.consulta(alumno_seleccionado.queryConsulta(alumno_seleccionado.id_alumno_a, alumno_seleccionado.ALUMNO, $"{alumno_seleccionado.WHERE}{alumno_seleccionado.nombre_alumno_a}{alumno_seleccionado.igual}{alumno_seleccionado.c}{cbAlumno.SelectedValue}{alumno_seleccionado.c}"), alumno_seleccionado.ALUMNO);
                    alumno_seleccionado.Id_alumno = alumno_seleccionado.obtenerDatos.Tables[0].Rows[0][0].ToString();
                }
                catch { }
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Horarios horario_actual = new Horarios(alumno_seleccionado.Id_alumno);
            horario_actual.buscar();
            txtHoraEntrada.Text = horario_actual.Hora_entrada.Remove(1,3);
            txtMinutoEntrada.Text = horario_actual.Hora_entrada.Remove(0, 2);
            txtHoraSalida.Text = horario_actual.Hora_salida.Remove(2,3);
            txtMinutoSalida.Text = horario_actual.Hora_salida.Remove(0, 3);
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            hora_entrada = txtHoraEntrada.Text + ":" + txtMinutoEntrada.Text;
            hora_salida = txtHoraSalida.Text + ":" + txtMinutoSalida.Text;
            Horarios horario_actual = new Horarios(alumno_seleccionado.Id_alumno,this);
            horario_actual.modificar();
        }

        private void cbAlumno_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtMinutoEntrada_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtMinutoSalida_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHoraSalida_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHoraEntrada_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
