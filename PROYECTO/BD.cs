﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace PROYECTO
{
    class BD
    {
        SQLiteConnection parametrosConexion = new SQLiteConnection("Data Source=baseCETI.db");
        SQLiteConnection conexionBD;
        SQLiteCommand queryCommand;
        public SQLiteDataAdapter adaptador;
        //SQLiteDataReader reader;
        SQLiteParameter picture;
        //SQLiteParameter picture2;
        public DataSet obtenerDatos;

        public bool conexionAbrir()
        {
            conexionBD = new SQLiteConnection(parametrosConexion);
            ConnectionState estadoConexion;
            try
            {
                conexionBD.Open();
                estadoConexion = conexionBD.State;
                return true;
            }
            catch (SQLiteException)
            {
                return false;
            }

        }

        public bool insertar(string sqliteInsertar)
        {
            int filasafectadas = 0;
            conexionAbrir();
            queryCommand = new SQLiteCommand(sqliteInsertar, conexionBD);
            filasafectadas = queryCommand.ExecuteNonQuery();
            conexionCerrar();
            if (filasafectadas > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool eliminar(string sqliteEliminar)
        {
            int filasafectadas = 0;
            conexionAbrir();
            queryCommand = new SQLiteCommand(sqliteEliminar,conexionBD);
            filasafectadas = queryCommand.ExecuteNonQuery();
            if (filasafectadas > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataSet consulta(string queryConsulta, string tabla)
        {
           conexionAbrir();
           obtenerDatos = new DataSet(); //se crea inatancia de DataSet
          
           adaptador = new SQLiteDataAdapter(queryConsulta, conexionBD); //adaptador recive losdatos 
           adaptador.Fill(obtenerDatos, tabla); //se da forma de dataset
           conexionCerrar();
           return obtenerDatos;
        }
        public void llenarComboBox(ComboBox llenarCombo, string sqlLlenarComboBox, string columna, string tabla, ref int entra)
        {
            obtenerDatos = consulta(sqlLlenarComboBox, tabla);
            entra = 1;
            llenarCombo.DataSource = obtenerDatos.Tables[tabla];
            llenarCombo.DisplayMember = columna;
            llenarCombo.ValueMember = columna;
            llenarCombo.SelectedIndex = -1;
            entra = 0;
        }

        public void llenarListBox(ListBox llenarlista, string queryLlenarListbox, string columna, string tabla, ref int entra)
        {
            //obtenerDatos = new DataSet();
            //obtenerDatos.Reset();
            obtenerDatos = consulta(queryLlenarListbox, tabla);
            //adaptador.Fill(obtenerDatos, tabla);
            entra = 1;
            llenarlista.DataSource = obtenerDatos.Tables[tabla];
            llenarlista.DisplayMember = columna;
            //llenarCombo.SelectedIndex = -1;
            entra = 0;
        }

        private void conexionCerrar()
        {
            if (conexionBD.State == ConnectionState.Open)
            {
                conexionBD.Close();
            }
        }

        public bool guardarImagen(Image imagen, /*System.Windows.Forms.PictureBox pbImagen2*/ int id)
        {
            if (imagen != null)
            {
                int filasAfectadas = 0;

                picture = new SQLiteParameter("@picture", SqlDbType.Image);
                //picture2 = new SQLiteParameter("@picture2", SqlDbType.Image);

                MemoryStream ms = new MemoryStream();
                MemoryStream ms2 = new MemoryStream();

                imagen.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                //pbImagen2.Image.Save(ms2, System.Drawing.Imaging.ImageFormat.Bmp);


                byte[] imagenByte = ms.GetBuffer();
                //byte[] imagenByte2 = ms2.GetBuffer();

                ms.Close();
                conexionAbrir();
                queryCommand = new SQLiteCommand(conexionBD);
                queryCommand.Parameters.Clear();
                queryCommand.Parameters.AddWithValue("@picture", imagenByte);
                //queryCommand.Parameters.AddWithValue("@picture2", imagenByte2);
                queryCommand.CommandText = "UPDATE `IMAGEN` SET imagen  = @picture WHERE `id_imagen` = " + id; //"INSERT INTO `IMAGEN`(`id_imagen`,`imagen`) VALUES (" + id + ", @picture)";
                /*"UPDATE `Visitante` SET `fotorostro` = (@picture) WHERE `idvisitante` = " + idVisitante + "";*/
                filasAfectadas = queryCommand.ExecuteNonQuery();
                conexionCerrar();

                if (filasAfectadas > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public bool actualizar(string sqlite_actualizar)
        {
            int filasafectadas = 0;
            conexionAbrir();
            queryCommand = new SQLiteCommand(sqlite_actualizar, conexionBD);
            filasafectadas = queryCommand.ExecuteNonQuery();
            conexionCerrar();
            if (filasafectadas > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool actualizar(string tabla, string datos, string condicion)
        {
            conexionAbrir();

            int filasAfectadas = 0;
            string queryActualizar = "UPDATE " + tabla + " SET " + datos + " WHERE " + condicion;

            queryCommand = new SQLiteCommand(queryActualizar, conexionBD);

            filasAfectadas = queryCommand.ExecuteNonQuery();

            conexionCerrar();

            if (filasAfectadas > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void llenarDgv(System.Windows.Forms.DataGridView dgvLlenar, string query, string tabla)
        {
            //obtenerDatos = new DataSet();
            //obtenerDatos.Reset();

            consulta(query, tabla);
           //adaptador.Fill(obtenerDatos, tabla);

            dgvLlenar.DataSource = obtenerDatos.Tables[0];
        }
    }
}
