﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROYECTO
{
    class Entrada_Salida : Query
    {

        #region ATRIBUTOS
        private string id_entrada_salida="";
        private string fecha = "";
        private string hora_entrada = "";
        private string esta_dentro="";
        private string hora_salida = "";
        private string id_alumnos = "";
        public string Esta_dentro
        {
            get
            {
                return esta_dentro;
            }

            set
            {
                esta_dentro = value;
            }
        }
        #endregion

        #region METODOS
        public string valores()
        {
            id_entrada_salida = maxIDEntradaSalidas();
            fecha = DateTime.Now.ToString("dd/MM/yyyy");
            return $"{abrirParentesis} {c}" +
                $"{id_entrada_salida} {ccc}" +
                $"{fecha} {ccc}" +
                $"{hora_entrada} {ccc}" +
                $"{Esta_dentro} {ccc}" +
                $"{hora_salida} {ccc}" +
                $"{id_alumnos} {c}" +
                $"{cerrarParantesis}";
        }
        public bool buscarLLegada(string id_alumno)
        {
            if(cargar(id_alumno))
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        public bool cargar(string id_alumno)
        {
            try
            {
                obtenerDatos = consulta(queryConsulta(ASTERISCO, ENTRADAS_SALIDAS, WHERE + fk_id_alumnos_a + igual + c + id_alumno + c + AND + fecha_a + igual + c + DateTime.Now.ToString("dd/MM/yyyy ") + c), ENTRADAS_SALIDAS);
                id_entrada_salida = obtenerDatos.Tables[0].Rows[0][0].ToString();
                fecha = obtenerDatos.Tables[0].Rows[0][1].ToString();
                hora_entrada = obtenerDatos.Tables[0].Rows[0][2].ToString();
                Esta_dentro = obtenerDatos.Tables[0].Rows[0][3].ToString();
                hora_salida = obtenerDatos.Tables[0].Rows[0][4].ToString();
                return true;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }
        public bool marcarSalida()
        {
            hora_salida = DateTime.Now.ToString("HH:mm");
            return (actualizar(queryActualizar(ENTRADAS_SALIDAS, hora_salida_a + igual + c + hora_salida + c + cc + esta_dentro_a + igual + c + "0" + c, WHERE + id_entrada_salida_a + igual + c + id_entrada_salida + c)));
        }
        public bool marcarLlegada(string id_alumno)
        {
            hora_entrada = DateTime.Now.ToString("HH:mm");
            Esta_dentro = "1";
            id_alumnos = id_alumno;
            return insertar(queryInsertar(ENTRADAS_SALIDAS, valores()));
        }
        public string maxIDEntradaSalidas()
        {
            try
            {
                obtenerDatos = consulta(queryConsulta(maxId(id_entrada_salida_a), ENTRADAS_SALIDAS, ""), ENTRADAS_SALIDAS);
                id_entrada_salida = (1 + Int32.Parse(obtenerDatos.Tables[ENTRADAS_SALIDAS].Rows[0][0].ToString())).ToString();
                return id_entrada_salida;
            }
            catch
            {
                return "1";
            }
        }
    }
    #endregion
}