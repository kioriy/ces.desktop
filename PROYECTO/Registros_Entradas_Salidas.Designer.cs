﻿namespace PROYECTO
{
    partial class Registros_Entradas_Salidas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registros_Entradas_Salidas));
            this.DgvRegistroEntradasSalidas = new System.Windows.Forms.DataGridView();
            this.DgvROWfecha_columna = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DgvRowNivelEstudios = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hora_entrada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dentro = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Hora_salida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.LblGrado = new System.Windows.Forms.Label();
            this.LblGrupo = new System.Windows.Forms.Label();
            this.CbxGrado = new System.Windows.Forms.ComboBox();
            this.CbxGrupo = new System.Windows.Forms.ComboBox();
            this.CalendarioRevisionRangoFechas = new System.Windows.Forms.MonthCalendar();
            this.cbxAlumnos = new System.Windows.Forms.ComboBox();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rbConFecha = new System.Windows.Forms.RadioButton();
            this.rbSinFecha = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.DgvRegistroEntradasSalidas)).BeginInit();
            this.SuspendLayout();
            // 
            // DgvRegistroEntradasSalidas
            // 
            this.DgvRegistroEntradasSalidas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvRegistroEntradasSalidas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DgvROWfecha_columna,
            this.nombre,
            this.apellido,
            this.DgvRowNivelEstudios,
            this.grado,
            this.grupo,
            this.hora_entrada,
            this.dentro,
            this.Hora_salida});
            this.DgvRegistroEntradasSalidas.Location = new System.Drawing.Point(3, 292);
            this.DgvRegistroEntradasSalidas.Margin = new System.Windows.Forms.Padding(4);
            this.DgvRegistroEntradasSalidas.Name = "DgvRegistroEntradasSalidas";
            this.DgvRegistroEntradasSalidas.ReadOnly = true;
            this.DgvRegistroEntradasSalidas.Size = new System.Drawing.Size(1257, 425);
            this.DgvRegistroEntradasSalidas.TabIndex = 0;
            // 
            // DgvROWfecha_columna
            // 
            this.DgvROWfecha_columna.HeaderText = "Fecha";
            this.DgvROWfecha_columna.Name = "DgvROWfecha_columna";
            this.DgvROWfecha_columna.ReadOnly = true;
            // 
            // nombre
            // 
            this.nombre.HeaderText = "Nombre(s) del Alumno";
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            // 
            // apellido
            // 
            this.apellido.HeaderText = "Apellido(s) del Alumno";
            this.apellido.Name = "apellido";
            this.apellido.ReadOnly = true;
            // 
            // DgvRowNivelEstudios
            // 
            this.DgvRowNivelEstudios.HeaderText = "Nivel De Estudios";
            this.DgvRowNivelEstudios.Name = "DgvRowNivelEstudios";
            this.DgvRowNivelEstudios.ReadOnly = true;
            // 
            // grado
            // 
            this.grado.HeaderText = "Grado";
            this.grado.Name = "grado";
            this.grado.ReadOnly = true;
            // 
            // grupo
            // 
            this.grupo.HeaderText = "Grupo";
            this.grupo.Name = "grupo";
            this.grupo.ReadOnly = true;
            // 
            // hora_entrada
            // 
            this.hora_entrada.HeaderText = "Hora de Entrada";
            this.hora_entrada.Name = "hora_entrada";
            this.hora_entrada.ReadOnly = true;
            // 
            // dentro
            // 
            this.dentro.HeaderText = "Esta en el plantel?";
            this.dentro.Name = "dentro";
            this.dentro.ReadOnly = true;
            this.dentro.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dentro.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Hora_salida
            // 
            this.Hora_salida.HeaderText = "Hora de Salida";
            this.Hora_salida.Name = "Hora_salida";
            this.Hora_salida.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label1.Location = new System.Drawing.Point(677, 96);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 29);
            this.label1.TabIndex = 6;
            // 
            // LblGrado
            // 
            this.LblGrado.AutoSize = true;
            this.LblGrado.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.LblGrado.Location = new System.Drawing.Point(539, 185);
            this.LblGrado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblGrado.Name = "LblGrado";
            this.LblGrado.Size = new System.Drawing.Size(86, 29);
            this.LblGrado.TabIndex = 9;
            this.LblGrado.Text = "Grado:";
            // 
            // LblGrupo
            // 
            this.LblGrupo.AutoSize = true;
            this.LblGrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.LblGrupo.Location = new System.Drawing.Point(705, 185);
            this.LblGrupo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblGrupo.Name = "LblGrupo";
            this.LblGrupo.Size = new System.Drawing.Size(86, 29);
            this.LblGrupo.TabIndex = 10;
            this.LblGrupo.Text = "Grupo:";
            // 
            // CbxGrado
            // 
            this.CbxGrado.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.CbxGrado.FormattingEnabled = true;
            this.CbxGrado.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.CbxGrado.Location = new System.Drawing.Point(544, 218);
            this.CbxGrado.Margin = new System.Windows.Forms.Padding(4);
            this.CbxGrado.Name = "CbxGrado";
            this.CbxGrado.Size = new System.Drawing.Size(52, 37);
            this.CbxGrado.TabIndex = 11;
            this.CbxGrado.SelectedIndexChanged += new System.EventHandler(this.CbxGrado_SelectedIndexChanged);
            // 
            // CbxGrupo
            // 
            this.CbxGrupo.Enabled = false;
            this.CbxGrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.CbxGrupo.FormattingEnabled = true;
            this.CbxGrupo.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.CbxGrupo.Location = new System.Drawing.Point(710, 218);
            this.CbxGrupo.Margin = new System.Windows.Forms.Padding(4);
            this.CbxGrupo.Name = "CbxGrupo";
            this.CbxGrupo.Size = new System.Drawing.Size(52, 37);
            this.CbxGrupo.TabIndex = 12;
            this.CbxGrupo.SelectedIndexChanged += new System.EventHandler(this.CbxGrupo_SelectedIndexChanged);
            // 
            // CalendarioRevisionRangoFechas
            // 
            this.CalendarioRevisionRangoFechas.BackColor = System.Drawing.Color.White;
            this.CalendarioRevisionRangoFechas.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.CalendarioRevisionRangoFechas.ForeColor = System.Drawing.SystemColors.WindowText;
            this.CalendarioRevisionRangoFechas.Location = new System.Drawing.Point(100, 71);
            this.CalendarioRevisionRangoFechas.Margin = new System.Windows.Forms.Padding(12, 11, 12, 11);
            this.CalendarioRevisionRangoFechas.Name = "CalendarioRevisionRangoFechas";
            this.CalendarioRevisionRangoFechas.ShowTodayCircle = false;
            this.CalendarioRevisionRangoFechas.TabIndex = 21;
            this.CalendarioRevisionRangoFechas.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.CalendarioRevisionRangoFechas_DateSelected);
            // 
            // cbxAlumnos
            // 
            this.cbxAlumnos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.cbxAlumnos.FormattingEnabled = true;
            this.cbxAlumnos.Location = new System.Drawing.Point(485, 122);
            this.cbxAlumnos.Margin = new System.Windows.Forms.Padding(4);
            this.cbxAlumnos.Name = "cbxAlumnos";
            this.cbxAlumnos.Size = new System.Drawing.Size(345, 37);
            this.cbxAlumnos.TabIndex = 22;
            this.cbxAlumnos.SelectedIndexChanged += new System.EventHandler(this.cbAlumnos_SelectedIndexChanged);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.BackgroundImage")));
            this.btnLimpiar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnLimpiar.FlatAppearance.BorderSize = 0;
            this.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpiar.Location = new System.Drawing.Point(1093, 71);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(115, 135);
            this.btnLimpiar.TabIndex = 23;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label2.Location = new System.Drawing.Point(480, 71);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 29);
            this.label2.TabIndex = 24;
            this.label2.Text = "Alumno:";
            // 
            // rbConFecha
            // 
            this.rbConFecha.AutoSize = true;
            this.rbConFecha.Checked = true;
            this.rbConFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.rbConFecha.Location = new System.Drawing.Point(100, 23);
            this.rbConFecha.Margin = new System.Windows.Forms.Padding(4);
            this.rbConFecha.Name = "rbConFecha";
            this.rbConFecha.Size = new System.Drawing.Size(145, 33);
            this.rbConFecha.TabIndex = 25;
            this.rbConFecha.TabStop = true;
            this.rbConFecha.Text = "Por Fecha";
            this.rbConFecha.UseVisualStyleBackColor = true;
            this.rbConFecha.CheckedChanged += new System.EventHandler(this.rbConFecha_CheckedChanged);
            // 
            // rbSinFecha
            // 
            this.rbSinFecha.AutoSize = true;
            this.rbSinFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.rbSinFecha.Location = new System.Drawing.Point(636, 13);
            this.rbSinFecha.Margin = new System.Windows.Forms.Padding(4);
            this.rbSinFecha.Name = "rbSinFecha";
            this.rbSinFecha.Size = new System.Drawing.Size(370, 33);
            this.rbSinFecha.TabIndex = 26;
            this.rbSinFecha.Text = "Desde el origen de los tiempos";
            this.rbSinFecha.UseVisualStyleBackColor = true;
            this.rbSinFecha.CheckedChanged += new System.EventHandler(this.rbSinFecha_CheckedChanged);
            // 
            // Registros_Entradas_Salidas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 719);
            this.Controls.Add(this.rbSinFecha);
            this.Controls.Add(this.rbConFecha);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.cbxAlumnos);
            this.Controls.Add(this.CalendarioRevisionRangoFechas);
            this.Controls.Add(this.CbxGrupo);
            this.Controls.Add(this.CbxGrado);
            this.Controls.Add(this.LblGrupo);
            this.Controls.Add(this.LblGrado);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DgvRegistroEntradasSalidas);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Registros_Entradas_Salidas";
            this.Text = "Registros_Entradas_Salidas";
            this.Load += new System.EventHandler(this.Registros_Entradas_Salidas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DgvRegistroEntradasSalidas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DgvRegistroEntradasSalidas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblGrado;
        private System.Windows.Forms.Label LblGrupo;
        private System.Windows.Forms.ComboBox CbxGrado;
        private System.Windows.Forms.ComboBox CbxGrupo;
        private System.Windows.Forms.MonthCalendar CalendarioRevisionRangoFechas;
        private System.Windows.Forms.DataGridViewTextBoxColumn DgvROWfecha_columna;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellido;
        private System.Windows.Forms.DataGridViewTextBoxColumn DgvRowNivelEstudios;
        private System.Windows.Forms.DataGridViewTextBoxColumn grado;
        private System.Windows.Forms.DataGridViewTextBoxColumn grupo;
        private System.Windows.Forms.DataGridViewTextBoxColumn hora_entrada;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dentro;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hora_salida;
        private System.Windows.Forms.ComboBox cbxAlumnos;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbConFecha;
        private System.Windows.Forms.RadioButton rbSinFecha;
    }
}