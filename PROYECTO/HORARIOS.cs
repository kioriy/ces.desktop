﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROYECTO
{
    class Horarios : Query
    {
        #region CONSTRUCTORES
        public Horarios(string id_alumno)
        {
            Fk_id_alumnos = id_alumno;
        }
        public Horarios(string id_alumno, FrmHorario llenar)
        {
            Fk_id_alumnos = id_alumno;
            Hora_entrada = llenar.hora_entrada;
            Hora_salida = llenar.hora_salida;
        }
        #endregion
        #region ATRIBUTOS
        private string id_horario = "";
        private string hora_entrada = "";
        private string hora_salida = "";
        private string fk_id_alumnos = "";
        #endregion
        
        #region VALORES
        private string valores()
        {
            return $"{abrirParentesis}" +
                $" NULL" +
                $"{cc}{c}{Hora_entrada}{ccc}" +
                $"{Hora_salida}{ccc}" +
                $"{Fk_id_alumnos}{c}" +
                $"{cerrarParantesis}";
        }
        private string valoresUpdate()
        {
            return //$"{id_horario_a}{igual}{c}{id_horario}{c}" +
                $"{hora_entrada_a}{igual}{c}{hora_entrada}{c}{cc}" +
                $"{hora_salida_a}{igual}{c}{hora_salida}{c}{cc}" +
                $"{fk_id_alumnos_a}{igual}{c}{fk_id_alumnos}{c}";
        }
        #endregion
        
        #region METODOS
        public bool insertarHorario()
        {
            insertar(queryInsertar(HORARIOS, valores()));
            return true;
        }
        public bool modificar()
        {
            return actualizar(queryActualizar(HORARIOS,valoresUpdate(),$"{WHERE}{fk_id_alumnos_a}{igual}{c}{fk_id_alumnos}{c}"));
        }
        public bool buscar()
        {
            obtenerDatos = consulta(queryConsulta($"{hora_entrada_a}{cc}{hora_salida_a}",HORARIOS,$"{WHERE}{fk_id_alumnos_a}{igual}{c}{Fk_id_alumnos}{c}"),HORARIOS);
            try
            {
                Hora_entrada = obtenerDatos.Tables[0].Rows[0][0].ToString();
                Hora_salida = obtenerDatos.Tables[0].Rows[0][1].ToString();
                obtenerDatos.Reset();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
        
        #region GET AND SET
        public string Id_horario
        {
            get
            {
                return id_horario;
            }

            set
            {
                id_horario = value;
            }
        }

        public string Hora_entrada
        {
            get
            {
                return hora_entrada;
            }

            set
            {
                hora_entrada = value;
            }
        }

        public string Hora_salida
        {
            get
            {
                return hora_salida;
            }

            set
            {
                hora_salida = value;
            }
        }

        public string Fk_id_alumnos
        {
            get
            {
                return fk_id_alumnos;
            }

            set
            {
                fk_id_alumnos = value;
            }
        }   
        #endregion
    }
}
