﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PROYECTO
{
    class Query : BD
    {
        #region INSTRUCCIONES SQL
        #region TABLAS
        public string ALUMNO = " ALUMNO ";
        public string ENTRADAS_SALIDAS   = " Entradas_salidas ";
        public string HORARIOS = " HORARIOS ";
        #endregion

        #region PALABRAS RESERVADAS SQL
        #region DESAMBIGUACIONES
        public string ALUMNOPUNTO = " ALUMNO.";
        public string ENTRADAS_SALIDASPUNTO = "Entradas_salidas.";
        public string TURNOSPUNTO = "TURNOS.";
        #endregion
        public string INSERT_INTO = "INSERT INTO ";
        public string INNER_JOIN = "INNER JOIN";
        public string ON = " ON ";
        public string BETWEEN = " BETWEEN ";
        public string ASTERISCO = " * ";
        public string igual = " = ";
        public string AND = " AND ";
        public string OR = " OR ";
        public string ORDEN_BY = " ORDER BY ";
        public string ASC = " ASC";
        public string WHERE = " WHERE ";
        public string MAX_ID = "MAX";
        public string LIKE = " LIKE ";
        public string AS = " AS ";

        public string abrirParentesis = "(";
        public string cerrarParantesis = ")";
        public string c = "'";
        public string nc = "'";
        public string cc = ",";
        public string ccc = "','";
        public string porciento = "%";
        #endregion
        #endregion

        #region ATRIBUTOS
        #region HORARIOS
        public string id_horario_a = "id_horario";
        #endregion
        #region ENTRADAS_SALIDAS
        public string id_entrada_salida_a = "id_entrada_salida";
        public string fecha_a = "Fecha";
        public string hora_entrada_a = "hora_entrada";
        public string esta_dentro_a = "esta_dentro";
        public string hora_salida_a = "hora_salida";
        public string fk_id_alumnos_a = "fk_id_alumnos";
        public string entradas_salidas_insertar = "('id_entrada_salida','Fecha','hora_entrada','esta_dentro','hora_salida','id_alumnos')";
        public string entradas_salidas_seleccionar = "'id_entrada_salida','Fecha','hora_entrada','esta_dentro','hora_salida','id_alumnos'";
        #endregion

        #region ALUMNO
       
        public string id_alumno_a = "id_alumnos";
        public string nombre_alumno_a = "nombre_alumno";
        public string apellido_alumno_a = "apellido_alumno";
        public string grado_a = "grado";
        public string grupo_a = "grupo";
        public string fk_id_turno_a = "fk_id_turno";
        public string tutor_a = "tutor";
        public string telefono_a = "telefono";
        public string telefono_2_a = "telefono_2";
        public string tipo_sangre_a = "tipo_sangre";
        public string nivel_estudios_a = "nivel_estudios";
        public string domicilio_a = "domicilio";
        public string cargo_a = "cargo";
        public string registro_a = "numero_lista";
        public string CURP_a = "CURP";
        public string codigo_a = "codigo";
        public string ruta_foto_a = "ruta_foto";
        public string alumno_atributos_insertar = "(`nombre_alumno`,`apellido_alumno`,`grado`,`grupo`,`tutor`,`telefono`,`telefono_2`,`tipo_sangre`,`nivel_estudios`,`domicilio`,`cargo`,`numero_lista`,`CURP`,`codigo`)";
        public string alumno_atributos_seleccionar = "`id_alumnos`,`nombre_alumno`,`apellido_alumno`,`grado`,`grupo`,`tutor`,`telefono`,`telefono_2`,`tipo_sangre`,`nivel_estudios`,`domicilio`,`cargo`,`numero_lista`,`CURP`,`codigo`";
        #endregion

        #endregion

        #region METODOS
        public string queryInsertar(string tabla,  string valores)
        {
            return "INSERT INTO " + tabla  + "VALUES" + valores;
        }
        
        public string queryConsulta(string atributo, string tabla, string condicion)
        {
            return "SELECT " + atributo + " FROM " + tabla + condicion;
        }

        public string queryActualizar(string tabla, string valores, string condicion)
        {
            return "UPDATE " + tabla + " SET " + valores + condicion;
        }

        public string queryEliminar(string tabla, string condicion)
        {
            return "DELETE * FROM " + tabla + " " + condicion;
        }
        public string consultaLike(string atributo, string tabla, string condicion)
        {
            return "SELECT " + atributo + " FROM " + tabla + " WHERE "+ atributo +" LIKE " + condicion ;
            //SELECT * FROM ALUMNO WHERE nombre_alumno LIKE '%D%';
        }

        public string maxId(string atributo)
        {
            return MAX_ID + abrirParentesis + atributo + cerrarParantesis;
        }
        #endregion
    }
}
