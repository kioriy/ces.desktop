﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PROYECTO
{
    public partial class Registros_Entradas_Salidas : Form
    {
        int entra = 0;
        #region CONSTRUCTOR

        public Registros_Entradas_Salidas()
        {
            InitializeComponent();
            modificarConsulta();
            rbConFecha.Checked = true;
            Alumno todos_los_alumnos = new Alumno();

            todos_los_alumnos.llenarComboBox(cbxAlumnos,todos_los_alumnos.queryConsulta($"{todos_los_alumnos.nombre_alumno_a}", todos_los_alumnos.ALUMNO, ""), todos_los_alumnos.nombre_alumno_a, todos_los_alumnos.ALUMNO,ref entra);
        }
        #endregion

        #region ATRIBUTOS
        Entrada_Salida es = new Entrada_Salida();
        string datos_a_obtener;
        private string condicion_consulta = "";
        #endregion

        #region EVENTOS
        private void Registros_Entradas_Salidas_Load(object sender, EventArgs e)
        {
            CalendarioRevisionRangoFechas.SelectionStart = ObtenerLunesAnterior(DateTime.Now);
            CalendarioRevisionRangoFechas.SelectionEnd = DateTime.Now;
            limpiarDGV();
            ocultarColumnasDGV();
            datos_a_obtener = es.fecha_a + es.cc
                + es.nombre_alumno_a + es.AS + es.c + "Nombre(s) del Alumno" + es.c + es.cc
                + es.apellido_alumno_a + es.AS + es.c + "Apellido(s) del Alumno" + es.c + es.cc
                + es.nivel_estudios_a + es.AS + es.c + "Nivel De Estudio" + es.c + es.cc
                + es.grado_a + es.AS + es.c + "Grado" + es.c + es.cc
                + es.grupo_a + es.AS + es.c + "Grupo" + es.c + es.cc
                + es.hora_entrada_a + es.AS + es.c + "Hora de Entrada" + es.c + es.cc
                + es.esta_dentro_a + es.AS + es.c + "Sigue en el plantel?" + es.c + es.cc
                + es.hora_salida_a + es.AS + es.c + "Hora de Salida" + es.c;
            condicion_consulta = $"{es.ON}{es.fk_id_alumnos_a}{es.igual}{es.id_alumno_a}{es.WHERE}{es.fecha_a}{es.BETWEEN}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.Start.ToString("dd/MM/yyyy")}{es.c}{es.AND}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.End.ToString("dd/MM/yyyy")}{es.c}";
            es.llenarDgv(DgvRegistroEntradasSalidas, es.queryConsulta(datos_a_obtener, es.ENTRADAS_SALIDAS + es.INNER_JOIN + es.ALUMNO, condicion_consulta), es.ENTRADAS_SALIDAS);
        }
        private void rbSinFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSinFecha.Checked)
            {
                CalendarioRevisionRangoFechas.Enabled = false;
                modificarConsulta();
                es.llenarDgv(DgvRegistroEntradasSalidas, es.queryConsulta(datos_a_obtener, es.ENTRADAS_SALIDAS + es.INNER_JOIN + es.ALUMNO, condicion_consulta), es.ENTRADAS_SALIDAS);
            }
        }

        private void rbConFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (rbConFecha.Checked)
            {
                CalendarioRevisionRangoFechas.Enabled = true;
                modificarConsulta();
                es.llenarDgv(DgvRegistroEntradasSalidas, es.queryConsulta(datos_a_obtener, es.ENTRADAS_SALIDAS + es.INNER_JOIN + es.ALUMNO, condicion_consulta), es.ENTRADAS_SALIDAS);
            }
        }
        private void CalendarioRevisionRangoFechas_DateSelected(object sender, DateRangeEventArgs e)
        {
            limpiarDGV();
            ocultarColumnasDGV();
            modificarConsulta();
            es.llenarDgv(DgvRegistroEntradasSalidas,es.queryConsulta(datos_a_obtener, es.ENTRADAS_SALIDAS + es.INNER_JOIN + es.ALUMNO, condicion_consulta), es.ENTRADAS_SALIDAS);
        }
        private void cbAlumnos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ( entra == 0)
            {
                modificarConsulta();
                es.llenarDgv(DgvRegistroEntradasSalidas, es.queryConsulta( datos_a_obtener, es.ENTRADAS_SALIDAS + es.INNER_JOIN + es.ALUMNO, condicion_consulta), es.ENTRADAS_SALIDAS);
                CbxGrado.Text = "";
                CbxGrupo.Text = "";
            }
        }
        private void CbxGrado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CbxGrado.Text !="" )
            {
                modificarConsulta();
                es.llenarDgv(DgvRegistroEntradasSalidas, es.queryConsulta(datos_a_obtener, es.ENTRADAS_SALIDAS + es.INNER_JOIN + es.ALUMNO, condicion_consulta), es.ENTRADAS_SALIDAS);
                cbxAlumnos.Text = "";
                CbxGrupo.Enabled = true;
            }
        }

        private void CbxGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra == 0)
            {
                modificarConsulta();

                es.llenarDgv(DgvRegistroEntradasSalidas, es.queryConsulta(datos_a_obtener, es.ENTRADAS_SALIDAS + es.INNER_JOIN + es.ALUMNO, condicion_consulta), es.ENTRADAS_SALIDAS);
                cbxAlumnos.Text = "";
            }
        }
        #endregion

        #region METODOS
        private void modificarConsulta()
        {
            if (rbConFecha.Checked)
            {
                condicion_consulta = $"{es.ON}{es.fk_id_alumnos_a}{es.igual}{es.id_alumno_a}{es.WHERE}{es.fecha_a}{es.BETWEEN}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.Start.ToString("dd/MM/yyyy ")}{es.c}{es.AND}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.End.ToString("dd/MM/yyyy ")}{es.c}";
                if (cbxAlumnos.Text!="")
                {
                    condicion_consulta = $"{es.ON}{es.fk_id_alumnos_a}{es.igual}{es.id_alumno_a}{es.WHERE}{es.fecha_a}{es.BETWEEN}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.Start.ToString("dd/MM/yyyy ")}{es.c}{es.AND}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.End.ToString("dd/MM/yyyy ")}{es.c}{es.AND}{es.nombre_alumno_a}{es.igual}{es.c}{cbxAlumnos.Text}{es.c}";
                }
                if (CbxGrado.Text!="")
                {
                    condicion_consulta = $"{es.ON}{es.fk_id_alumnos_a}{es.igual}{es.id_alumno_a}{es.WHERE}{es.fecha_a}{es.BETWEEN}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.Start.ToString("dd/MM/yyyy ")}{es.c}{es.AND}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.End.ToString("dd/MM/yyyy ")}{es.c}{es.AND}{es.grado_a}{es.igual}{es.c}{CbxGrado.Text}{es.c}";
                }
                if (CbxGrupo.Text!="")
                {
                    condicion_consulta = $"{es.ON}{es.fk_id_alumnos_a}{es.igual}{es.id_alumno_a}{es.WHERE}{es.fecha_a}{es.BETWEEN}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.Start.ToString("dd/MM/yyyy ")}{es.c}{es.AND}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.End.ToString("dd/MM/yyyy ")}{es.c}{es.AND}{es.grado_a}{es.igual}{es.c}{CbxGrado.Text}{es.c}{es.AND}{es.grupo_a}{es.igual}{es.c}{CbxGrupo.Text}{es.c}";
                }
            }
            if (rbSinFecha.Checked)
            {
                condicion_consulta = $"{es.ON}{es.fk_id_alumnos_a}{es.igual}{es.id_alumno_a}";
                if (cbxAlumnos.Text != "")
                {
                    condicion_consulta = $"{es.ON}{es.fk_id_alumnos_a}{es.igual}{es.id_alumno_a}{es.WHERE}{es.nombre_alumno_a}{es.igual}{es.c}{cbxAlumnos.SelectedValue}{es.c}";
                }
                if (CbxGrado.Text != "")
                {
                    condicion_consulta = $"{es.ON}{es.fk_id_alumnos_a}{es.igual}{es.id_alumno_a}{es.WHERE}{es.grado_a}{es.igual}{es.c}{CbxGrado.SelectedItem}{es.c}";//{es.ORDEN_BY}{es.fecha_a}
                }
                if (CbxGrupo.Text != "")
                {
                    condicion_consulta = $"{es.ON}{es.fk_id_alumnos_a}{es.igual}{es.id_alumno_a}{es.WHERE}{es.grado_a}{es.igual}{es.c}{CbxGrado.SelectedItem}{es.c}{es.AND}{es.grupo_a}{es.igual}{es.c}{CbxGrupo.SelectedItem}{es.c}";
                }
            }
        }
        public void ocultarColumnasDGV()
        {
            DgvROWfecha_columna.Visible = false;
            DgvRowNivelEstudios.Visible = false;
            nombre.Visible = false;
            apellido.Visible = false;
            grado.Visible = false;
            grupo.Visible = false;
            hora_entrada.Visible = false;
            dentro.Visible = false;
            Hora_salida.Visible = false;
        }
        private void limpiarDGV()
        {
            for (int numero_filas_eliminar = DgvRegistroEntradasSalidas.RowCount - 1; numero_filas_eliminar > 0; numero_filas_eliminar--)
            {
                DgvRegistroEntradasSalidas.Rows.RemoveAt(numero_filas_eliminar - 1);
            }
        }
        public static DateTime ObtenerLunesAnterior(DateTime fecha)
        {
            int dia = Convert.ToInt32(fecha.DayOfWeek);
            dia = dia - 1;
            DateTime fechaInicioSemana = fecha.AddDays((dia) * (-1));
            return fechaInicioSemana;
        }
        #endregion

        #region BOTONES
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cbxAlumnos.Text = "";
            CbxGrado.SelectedIndex = -1;
            CbxGrupo.SelectedIndex = -1;
            CbxGrupo.Enabled = false;
            CalendarioRevisionRangoFechas.SelectionStart = ObtenerLunesAnterior(DateTime.Now);
            CalendarioRevisionRangoFechas.SelectionEnd = DateTime.Now;
            limpiarDGV();
            ocultarColumnasDGV();
           // condicion_consulta = $"{es.ON}{es.fk_id_alumnos_a}{es.igual}{es.id_alumno_a}{es.WHERE}{es.fecha_a}{es.BETWEEN}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.Start.ToString("dd/MM/yyyy ")}{es.c}{es.AND}{es.c}{CalendarioRevisionRangoFechas.SelectionRange.End.ToString("dd/MM/yyyy ")}{es.c}";
           // es.llenarDgv(DgvRegistroEntradasSalidas, es.queryConsulta(datos_a_obtener, es.ENTRADAS_SALIDAS + es.INNER_JOIN + es.ALUMNO, condicion_consulta), es.ENTRADAS_SALIDAS);
        }
        #endregion
    }
}