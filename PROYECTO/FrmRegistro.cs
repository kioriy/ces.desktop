﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
      
namespace PROYECTO
{
    public partial class FrmRegistro : Form
    {
        #region ATRIBUTOS
        Alumno alumno = new Alumno();
        int entra = 0;
        #endregion

        #region CONSTRUCTOR
        public FrmRegistro()
        {
            InitializeComponent();
           // alumno.cargarComboBoxAlumno(cbNombre, alumno.nombre_alumno_a, ref entra);
           // alumno.cargarComboBoxAlumno(cbApellidos, alumno.apellido_alumno_a, ref entra);
        }
        #endregion

        #region METODOS
        void  RightWrong(bool action)
        {
            if (action)
            {
                MessageBox.Show("Acción Exitosa ");
            }
            else
            {
                MessageBox.Show("Acción Inocorrecta");
            }
        }

            /// <summary>
        /// hace visible los listbox
        /// </summary>
        /// <param name="on_of"></param>()
        void ViewList(bool on_of )
        {
            if (on_of)
            {
                lbnombre.Visible = true;
                lbapellido.Visible = true;
            }
            else
            {
                lbnombre.Visible = false;
                lbapellido.Visible = false;
            }
        }


        void CargarCampos()
        {
           
            alumno.Nombre_alumno = TxtNombre.Text;
            alumno.Apellido_alumno = TxtApellido.Text;
            alumno.Domicilio = TxtDomicilio.Text;
            alumno.Tutor = TxtTutor.Text;
            alumno.Telefono = TxtTelefono.Text;
            alumno.Telefono_2 = TxtTelefono2.Text;
            alumno.Grado = TxtGrado.Text;
            alumno.Grupo = TxtGrupo.Text;
            alumno.Registro = TxtRegistro.Text;
            alumno.Nivel_estudios = CbNivelEstudio.Text;
            alumno.Tipo_sangre = CbTipoSangre.Text;
            alumno.CURP1 = lBturnos.Text;
        }


        void llenarCampos()
        {
            TxtNombre.Text = alumno.Nombre_alumno;
            TxtApellido.Text = alumno.Apellido_alumno;
            TxtDomicilio.Text = alumno.Domicilio;
            TxtTutor.Text = alumno.Tutor;
            TxtTelefono.Text = alumno.Telefono;
            TxtTelefono2.Text = alumno.Telefono_2;
            TxtGrado.Text = alumno.Grado;
            TxtGrupo.Text = alumno.Grupo;
            TxtRegistro.Text = alumno.Registro;
            CbNivelEstudio.Text = alumno.Nivel_estudios;
            CbTipoSangre.Text = alumno.Tipo_sangre;
            lBturnos.Text = alumno.CURP1;
        }

        void limpiarCampos()
        {
            TxtNombre.Clear();
            TxtApellido.Clear();
            TxtDomicilio.Clear();
            TxtTutor.Clear();
            TxtTelefono.Clear();
            TxtTelefono2.Clear();
            TxtGrado.Clear();
            TxtGrupo.Clear();
            TxtRegistro.Clear();
            CbNivelEstudio.SelectedIndex = -1;
            CbTipoSangre.SelectedIndex = -1;
            TxtCURP.Clear();
            lBturnos.ClearSelected();
            //lbnombre.TopIndex = lbnombre.SelectedIndex;
            //// Remove all items before the top item in the ListBox.
            //for (int x = (lbnombre.SelectedIndex - 1); x >= 0; x--)
            //{
            //    lbnombre.Items.RemoveAt(x);
            //}
        }
        #endregion

        #region BOTONES
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (lBturnos.Text == "" || TxtTutor.Text == "" || TxtGrado.Text == "" || TxtGrupo.Text == "" || TxtDomicilio.Text == "" || TxtTelefono.Text == "" || TxtNombre.Text == "" || TxtApellido.Text == "")
            {
                if (TxtTutor.Text == "" || TxtGrado.Text == "" || TxtGrupo.Text == "" || TxtDomicilio.Text == "" || TxtTelefono.Text == "" || TxtNombre.Text == "" || TxtApellido.Text == "")
                {
                    MessageBox.Show("Camo(s) vacio(s)");
                }
                else if (lBturnos.Text == "")
                {
                    MessageBox.Show("Seleccione un turno");
                }   
            }
            else
            {
                Alumno a = new Alumno(this);
                if (a.InsertarAlumno())
                {
                    
                    MessageBox.Show("Registro insertado correctamente");
                    limpiarCampos();
                }
                else
                {
                    MessageBox.Show("El registro no se inserto, intente nuevamente");
                }
            }
        
         

        }
        private void BtnRevisarRegistros_Click(object sender, EventArgs e)
        {
            Registros_Entradas_Salidas Registros = new Registros_Entradas_Salidas();
            Registros.Show();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            if (alumno.cargar(TxtNombre.Text))
            {
                llenarCampos();
            }
            
            {
                MessageBox.Show("Alumno no encontrado", "Registro inexistente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (alumno.cargar(TxtNombre.Text))
            {
                alumno.eliminar(alumno.queryEliminar(alumno.ALUMNO, $"{alumno.WHERE}{alumno.codigo_a}{alumno.igual}{TxtCURP.Text}"));
                alumno.eliminar(alumno.queryEliminar(alumno.ENTRADAS_SALIDAS, $"{alumno.WHERE}{alumno.fk_id_alumnos_a}{alumno.igual}{alumno.Id_alumno}"));
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {

            // Alumno alumno_a_modificar = new Alumno(this);
            // int index = lbapellido.SelectedIndex; 
            //alumno.cargar(alumno.lista_id_alumnos[index]);
            //alumno = new Alumno();
            if (lBturnos.Text == "" || TxtTutor.Text == "" || TxtGrado.Text == "" || TxtGrupo.Text == "" || TxtDomicilio.Text == "" || TxtTelefono.Text == "" || TxtNombre.Text == "" || TxtApellido.Text == "")
            {
                if (TxtTutor.Text == "" || TxtGrado.Text == "" || TxtGrupo.Text == "" || TxtDomicilio.Text == "" || TxtTelefono.Text == "" || TxtNombre.Text == "" || TxtApellido.Text == "")
                {
                    MessageBox.Show("Camo(s) vacio(s)");
                }
                else if (lBturnos.Text == "")
                {
                    MessageBox.Show("Seleccione un turno");
                }

            }
            else
            {

                ViewList(false);
                CargarCampos();
                //alumno.actualizar();
                RightWrong(alumno.actualizar());
                limpiarCampos();
            }

        }
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }
        private void btnMonitor_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            //this.ShowInTaskbar = false;
            FrmMonitor mostrar = new FrmMonitor();
            mostrar.Show();
            //this.Visible = true;
            //this.ShowInTaskbar = true;
        }
        #endregion

        private void TxtNombre_TextChanged(object sender, EventArgs e)
        {       
            buscarAlumno(TxtNombre.Text);
            if (lbapellido.Items.Count == 0)
            {
                ViewList(false);
            }
          //  if (TxtTutor != null && TxtGrado != null && TxtGrupo != null && TxtDomicilio != null && TxtTelefono != null)
            //{
              //  ViewList(false);
            //}
            /* alumno.llenarListBox(lbnombre, alumno.nombre_alumno_a, TxtNombre.Text, ref entra);
             alumno.llenarListBox(lbapellido, alumno.apellido_alumno_a, TxtNombre.Text, ref entra);
             alumno.llenarListaIdAlumno(TxtNombre.Text);

             if (TxtNombre.Text == "")
             {
                 ViewList(false);
             }
             else
             {
                 ViewList(true);
             }*/
        }

        private void TxtApellido_TextChanged_1(object sender, EventArgs e)
        {
            buscarAlumno(TxtApellido.Text);
            if (lbapellido.Items.Count == 0)
            {
                ViewList(false);
            }
        }

        private void lbnombre_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (entra == 0)
            {
                int index = lbnombre.SelectedIndex; //alumno.lista_id_alumnos.IndexOf(lbnombre.Text);
                alumno.cargar(alumno.lista_id_alumnos[index]);
                cargarFormalario();
             //   ViewList(TxtNombre.Text);
            }
        }

        private void lbapellido_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra == 0)
            {
                int index = lbapellido.SelectedIndex; //alumno.lista_id_alumnos.IndexOf(lbnombre.Text);
                alumno.cargar(alumno.lista_id_alumnos[index]);
                cargarFormalario();
               // NoViewList(TxtApellido.Text);
            }
        }

        public void buscarAlumno(string nombre_apellido)
        {
            alumno.llenarListBox(lbnombre, alumno.nombre_alumno_a, nombre_apellido, ref entra);
            alumno.llenarListBox(lbapellido, alumno.apellido_alumno_a, nombre_apellido, ref entra);
            alumno.llenarListaIdAlumno(nombre_apellido);


            if (nombre_apellido == "")
            {
                ViewList(false);
            }
            else
            {
                ViewList(true);
            }
        }

        private void cargarFormalario()
        {
            TxtNombre.Text = alumno.Nombre_alumno;
            TxtApellido.Text = alumno.Apellido_alumno;
            TxtTutor.Text = alumno.Tutor;
            TxtDomicilio.Text = alumno.Domicilio;
            TxtTelefono.Text = alumno.Telefono;
            TxtGrado.Text = alumno.Grado;
            TxtGrupo.Text = alumno.Grupo;
            lBturnos.Text = alumno.CURP1;
            String idalumno = alumno.Id_alumno;
            if (TxtNombre.Text == "")
            {
                ViewList(true);
            }
            else
            {
                ViewList(false);
            }

        }

        private void FrmRegistro_Load(object sender, EventArgs e)
        {

        }

        private void TxtTutor_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void TxtTutor_Enter(object sender, EventArgs e)
        {

            ViewList(false);

        }

        private void TxtDomicilio_Enter(object sender, EventArgs e)
        {
            ViewList(false);
        }

        private void TxtTelefono_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtTelefono_Enter(object sender, EventArgs e)
        {
            ViewList(false);
        }

        private void TxtGrado_Enter(object sender, EventArgs e)
        {
            ViewList(false);
        }

        private void TxtGrupo_Enter(object sender, EventArgs e)
        {
            ViewList(false);
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
