﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PROYECTO
{
    public partial class FrmMonitor : Form
    {
        #region CONSTRUCTOR
        public FrmMonitor()
        {
            InitializeComponent();
           
        }
        #endregion

        #region ATRIBUTOS
        Alumno alumno = new Alumno();

        #endregion

        #region EVENTOS
        
        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
       
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                Validaciones en_horario = new Validaciones();

                alumno.Id_alumno = txtCodigo.Text;

                String id_alumnoentra = txtCodigo.Text;

             




                if (alumno.cargar(alumno.Id_alumno))
                {
                    //Horarios valida = new Horarios(alumno.Id_alumno);

                    //if (valida.buscar())
                    //{
                    // if (en_horario.ValidacionMarcar(valida.Hora_entrada, valida.Hora_salida))
                    //{
                    Entrada_Salida Marca = new Entrada_Salida();

                    if (Marca.buscarLLegada(alumno.Id_alumno))
                    {
                        if (bool.Parse(Marca.Esta_dentro))
                        {
                            Marca.marcarSalida();
                            pbEntrada.Visible = false;
                            lNotificaciones.Text = "Salida";
                        }
                        else
                        {
                            lNotificaciones.Text = "El alumno ya marco su salida el día de hoy";//MessageBox.Show("El alumno ya marco su salida el día de hoy", "Error", System.Windows.Forms.MessageBoxButtons.OK);
                            pbEntrada.Visible = false;
                        }
                    }
                    else
                    {
                       Marca.marcarLlegada(alumno.Id_alumno);
                       pbEntrada.Visible = true;
                       lNotificaciones.Text = "Llegada";
                    }
                    llenarCampos();
                    // }
                    // else
                    // {
                    //     MessageBox.Show("Entrada o Salida Invalida", "Sale o entra tarde", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    // }
                    //}
                    //else
                    //{  
                    //    MessageBox.Show("El alumno existe pero no tiene un horario registrado", "Error");
                    //}
                }
                else
                {
                    lNotificaciones.Text = "Alumno no encontrado";// MessageBox.Show("Alumno no encontrado", "Registro inexistente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                txtCodigo.Clear();
            }
        }
        #endregion

        #region METODOS
        void llenarCampos()
        {
            pbxFotoAlumno.Image = null;
            lblnombre.Text = alumno.Nombre_alumno;
            lblapellidos.Text = alumno.Apellido_alumno;
            lblgrado.Text = alumno.Grado;
            lblgrupo.Text = alumno.Grupo;
            String imgnombres = alumno.Apellido_alumno + " " + alumno.Nombre_alumno;
            String Turnos = alumno.CURP1;
            try
            {



                pbxFotoAlumno.Image = Image.FromFile($"Imagenes/TURNOS/{Turnos}/GRADOS/{alumno.Grado}/GRUPOS/{ alumno.Grupo}/{imgnombres}.jpg");
                //pbxFotoAlumno.Image = Image.FromFile($"Imagenes/{txtCodigo.Text}.png");
            }
            catch
            {
                lNotificaciones.Text = "Imagen no encontrada";//MessageBox.Show("Imagen no encontrada", "Sin imagen", MessageBoxButtons.OK);
                
            }
        }
        #endregion

        private void lblgrupo_Click(object sender, EventArgs e)
        {

        }

        private void FrmMonitor_Load(object sender, EventArgs e)
        {

        }

        private void pbEntrada_Click(object sender, EventArgs e)
        {

        }

        private void pbxFotoAlumno_Click(object sender, EventArgs e)
        {

        }
    }
}