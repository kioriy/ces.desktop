﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROYECTO
{
    class Validaciones
    {
        public bool ValidacionMarcar(string hora_entrada, string hora_salida)
        {
            if (hora_entrada.Length == 5)
            {
                return (int.Parse(hora_entrada.Remove(2, 1)) < int.Parse(DateTime.Now.ToString("HHmm"))
                && int.Parse(hora_salida.Remove(2, 1)) > int.Parse(DateTime.Now.ToString("HHmm")));
            }
            else
            {
                return (int.Parse(hora_entrada.Remove(1, 1)) < int.Parse(DateTime.Now.ToString("HHmm"))
                && int.Parse(hora_salida.Remove(2, 1)) > int.Parse(DateTime.Now.ToString("HHmm")));
            }
        }
    }
}
