﻿namespace PROYECTO
{
    partial class FrmMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMonitor));
            this.lblnombre = new System.Windows.Forms.Label();
            this.lblgrupo = new System.Windows.Forms.Label();
            this.pbxFotoAlumno = new System.Windows.Forms.PictureBox();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.lblgrado = new System.Windows.Forms.Label();
            this.lblapellidos = new System.Windows.Forms.Label();
            this.pbSalida = new System.Windows.Forms.PictureBox();
            this.pbEntrada = new System.Windows.Forms.PictureBox();
            this.lNotificaciones = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFotoAlumno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSalida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEntrada)).BeginInit();
            this.SuspendLayout();
            // 
            // lblnombre
            // 
            this.lblnombre.AutoSize = true;
            this.lblnombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnombre.Location = new System.Drawing.Point(366, 51);
            this.lblnombre.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblnombre.Name = "lblnombre";
            this.lblnombre.Size = new System.Drawing.Size(232, 69);
            this.lblnombre.TabIndex = 32;
            this.lblnombre.Text = "nombre";
            // 
            // lblgrupo
            // 
            this.lblgrupo.AutoSize = true;
            this.lblgrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgrupo.Location = new System.Drawing.Point(562, 326);
            this.lblgrupo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblgrupo.Name = "lblgrupo";
            this.lblgrupo.Size = new System.Drawing.Size(196, 69);
            this.lblgrupo.TabIndex = 42;
            this.lblgrupo.Text = "Grupo";
            this.lblgrupo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblgrupo.Click += new System.EventHandler(this.lblgrupo_Click);
            // 
            // pbxFotoAlumno
            // 
            this.pbxFotoAlumno.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbxFotoAlumno.InitialImage = null;
            this.pbxFotoAlumno.Location = new System.Drawing.Point(28, 47);
            this.pbxFotoAlumno.Margin = new System.Windows.Forms.Padding(4);
            this.pbxFotoAlumno.Name = "pbxFotoAlumno";
            this.pbxFotoAlumno.Size = new System.Drawing.Size(300, 400);
            this.pbxFotoAlumno.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxFotoAlumno.TabIndex = 2;
            this.pbxFotoAlumno.TabStop = false;
            this.pbxFotoAlumno.Click += new System.EventHandler(this.pbxFotoAlumno_Click);
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.SystemColors.Control;
            this.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigo.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtCodigo.Location = new System.Drawing.Point(28, 15);
            this.txtCodigo.Margin = new System.Windows.Forms.Padding(4);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(297, 15);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigo_KeyPress);
            // 
            // lblgrado
            // 
            this.lblgrado.AutoSize = true;
            this.lblgrado.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgrado.Location = new System.Drawing.Point(366, 326);
            this.lblgrado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblgrado.Name = "lblgrado";
            this.lblgrado.Size = new System.Drawing.Size(196, 69);
            this.lblgrado.TabIndex = 43;
            this.lblgrado.Text = "Grado";
            this.lblgrado.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblapellidos
            // 
            this.lblapellidos.AutoSize = true;
            this.lblapellidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblapellidos.Location = new System.Drawing.Point(366, 185);
            this.lblapellidos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblapellidos.Name = "lblapellidos";
            this.lblapellidos.Size = new System.Drawing.Size(244, 69);
            this.lblapellidos.TabIndex = 33;
            this.lblapellidos.Text = "Apellido";
            // 
            // pbSalida
            // 
            this.pbSalida.Image = ((System.Drawing.Image)(resources.GetObject("pbSalida.Image")));
            this.pbSalida.Location = new System.Drawing.Point(1157, 250);
            this.pbSalida.Margin = new System.Windows.Forms.Padding(4);
            this.pbSalida.Name = "pbSalida";
            this.pbSalida.Size = new System.Drawing.Size(172, 159);
            this.pbSalida.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSalida.TabIndex = 58;
            this.pbSalida.TabStop = false;
            // 
            // pbEntrada
            // 
            this.pbEntrada.Image = ((System.Drawing.Image)(resources.GetObject("pbEntrada.Image")));
            this.pbEntrada.Location = new System.Drawing.Point(1158, 250);
            this.pbEntrada.Margin = new System.Windows.Forms.Padding(4);
            this.pbEntrada.Name = "pbEntrada";
            this.pbEntrada.Size = new System.Drawing.Size(172, 159);
            this.pbEntrada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbEntrada.TabIndex = 59;
            this.pbEntrada.TabStop = false;
            this.pbEntrada.Click += new System.EventHandler(this.pbEntrada_Click);
            // 
            // lNotificaciones
            // 
            this.lNotificaciones.AutoSize = true;
            this.lNotificaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lNotificaciones.Location = new System.Drawing.Point(20, 482);
            this.lNotificaciones.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lNotificaciones.Name = "lNotificaciones";
            this.lNotificaciones.Size = new System.Drawing.Size(229, 46);
            this.lNotificaciones.TabIndex = 60;
            this.lNotificaciones.Text = "Notificiones";
            // 
            // FrmMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1646, 547);
            this.Controls.Add(this.lNotificaciones);
            this.Controls.Add(this.pbEntrada);
            this.Controls.Add(this.pbSalida);
            this.Controls.Add(this.lblnombre);
            this.Controls.Add(this.lblapellidos);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.lblgrado);
            this.Controls.Add(this.pbxFotoAlumno);
            this.Controls.Add(this.lblgrupo);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmMonitor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Monitor";
            this.Load += new System.EventHandler(this.FrmMonitor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxFotoAlumno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSalida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEntrada)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblnombre;
        private System.Windows.Forms.Label lblgrupo;
        private System.Windows.Forms.PictureBox pbxFotoAlumno;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label lblgrado;
        private System.Windows.Forms.Label lblapellidos;
        private System.Windows.Forms.PictureBox pbSalida;
        private System.Windows.Forms.PictureBox pbEntrada;
        private System.Windows.Forms.Label lNotificaciones;
    }
}