--  Creado con Kata Kuntur - Modelador de Datos
--  Versi�n: 2.9.1
--  Sitio Web: http://katakuntur.jeanmazuelos.com/
--  Si usted encuentra alg�n error le agradeceriamos lo reporte en:
--  http://pm.jeanmazuelos.com/katakuntur/issues/new

--  Administrador de Base de Datos: SQLite
--  Diagrama: bd
--  Autor: Diego
--  Fecha y hora: 28/11/2016 21:30:27
PRAGMA foreign_keys = ON;

-- GENERANDO TABLAS
CREATE TABLE ALUMNOS (
	id_alumnos INTEGER PRIMARY KEY AUTOINCREMENT   NOT NULL ,
	nombre_alumno varCHAR(50) NOT NULL ,
	apellido_alumno varCHAR(50) NOT NULL ,
	grado VARCHAR(10) NOT NULL ,
	grupo chARACTER(1) NOT NULL ,
	tutor varCHAR(100) NOT NULL ,
	telefono vaRCHAR(10) NOT NULL ,
	telefono_2 vARCHAR(10) NOT NULL ,
	tipo_sangre vaRCHAR(4) NOT NULL ,
	nivele_estudios vaRCHAR(25) NOT NULL ,
	domicilio varCHAR(100) NOT NULL ,
	cargo vARCHAR(10) NOT NULL ,
	numero_lista INTEGER NOT NULL ,
	CURP varCHAR(20) NOT NULL ,
	codigo varCHAR(25) NOT NULL 
);