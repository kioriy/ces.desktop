--  Creado con Kata Kuntur - Modelador de Datos
--  Versi�n: 2.9.1
--  Sitio Web: http://katakuntur.jeanmazuelos.com/
--  Si usted encuentra alg�n error le agradeceriamos lo reporte en:
--  http://pm.jeanmazuelos.com/katakuntur/issues/new

--  Administrador de Base de Datos: SQLite
--  Diagrama: bd
--  Autor: Diego
--  Fecha y hora: 28/11/2016 22:19:42
PRAGMA foreign_keys = ON;

-- GENERANDO TABLAS
CREATE TABLE ALUMNO (
	id_alumnos INTEGER PRIMARY KEY AUTOINCREMENT   NOT NULL ,
	nombre_alumno varCHAR(50) NOT NULL ,
	apellido_alumno vARCHAR(50) NOT NULL ,
	grado VARCHAR(5) NOT NULL ,
	grupo VARCHAR(10) NOT NULL ,
	telefono INTEGER NOT NULL ,
	telefono_2 INTEGER NOT NULL ,
	tipo_sangre vARCHAR(5) NOT NULL ,
	nivel_estudios vaRCHAR(25) NOT NULL ,
	domicilio VARCHAR(100) NOT NULL ,
	cargo VARCHAR(25) NOT NULL ,
	numero_lista VARCHAR(20) NOT NULL ,
	CURP VARCHAR(20) NOT NULL ,
	codigo VARCHAR(50) NOT NULL 
);