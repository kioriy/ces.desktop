﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PROYECTO
{
    class Alumno : Query
    {
        private string id_alumno = "";
        private string nombre_alumno = "";
        private string apellido_alumno = "";
        private string grado = "";
        private string grupo = "";
        //private string hora_entrada = "";
        //private string hora_salida = "";
        private string tutor = "";
        private string telefono = "";
        private string telefono_2 = "";
        private string tipo_sangre = "";
        private string nivel_estudios = "SECUNDARIA";
        private string domicilio = "";
        //private string cargo = "";
        private string registro = "";
        private string CURP = "";
        //private string codigo = "";
        /*private string ruta_foto = "";
        private string ruta_frente = "";
        private string ruta_vuelta = "";*/
        public List<string> lista_id_alumnos { get; set; } = new List<string>();
        //public List<string> lista_id_alumnos = new List<string>() { get; set; }
        //List<string> lista_id_alumnos = new List<string>();
        //public List<string> lista_id_alumno = new List<string>();

        //Foto foto;

        private string tabla = "";
        private string atributos_insertar = "";
        private string atributos_seleccionar = "";

        #region CONSTRUCTORES

        public Alumno(FrmRegistro a)
        {

            
            //this.id_alumno = id_alumno;
            this.nombre_alumno = a.TxtNombre.Text.Trim();
            this.apellido_alumno = a.TxtApellido.Text.Trim();
            this.grado = a.TxtGrado.Text.Trim();
            this.grupo = a.TxtGrupo.Text.Trim();
            //this.hora_entrada = a.txtHoraDeEntrada.Text;
            //this.hora_salida = a.txtHoraDeSalida.Text;
            this.tutor = a.TxtTutor.Text.Trim();
            this.telefono = a.TxtTelefono.Text.Trim();
            this.telefono_2 = "";//a.TxtTelefono2.Text;
            this.tipo_sangre = "";//a.CbTipoSangre.Text;
            this.nivel_estudios = "SECUNDARIA";//a.CbNivelEstudio.Text;
            this.domicilio = a.TxtDomicilio.Text.Trim();
            //this.cargo = a.CbCargo.Text;
            this.registro = "";//a.TxtRegistro.Text;
            this.CURP1 = a.lBturnos.Text;//a.TxtCURP.Text.Trim();
            //this.Codigo = a.TxtCodigo.Text;
            /*obtenerDatos = consulta(queryConsulta(id_turno_a,TURNOS,$"{WHERE}{turno_a}{igual}{c}{a.CbTurno.Text}{c}"),TURNOS);
            this.fk_id_turno = obtenerDatos.Tables[0].Rows[0][0].ToString();*/
            //this.foto = foto;

            this.tabla = ALUMNO;
            this.atributos_insertar = alumno_atributos_insertar;
            this.atributos_seleccionar = alumno_atributos_seleccionar;

        }

        /*public Alumno(Foto foto)
        {
            this.foto = foto;

            tabla = ALUMNO;
            atributos_insertar = alumno_atributos_insertar;
            atributos_seleccionar = alumno_atributos_seleccionar;
        }*/

        public Alumno()
        {
            tabla = ALUMNO;
            atributos_insertar = alumno_atributos_insertar;
            atributos_seleccionar = alumno_atributos_seleccionar;
        }
        #endregion

        #region VALORES
        public string valoresUpdate()
        {


            return
            $"{nombre_alumno_a}{igual} {c}{nombre_alumno.ToUpper()}{c}{cc}" +
            $"{apellido_alumno_a}{igual}{c}{apellido_alumno.ToUpper()}{c}{cc}" +
            $"{grado_a} {igual} {c}{grado.ToUpper()}{c} {cc}" +
            $"{grupo_a} {igual} {c}{grupo.ToUpper()}{c} {cc}" +
            //$"{hora_entrada_a} {igual} {c}{hora_entrada}{c} {cc}"+
            //$"{hora_salida_a}{igual} {c}{hora_salida}{c}{cc}"+
            $"{tutor_a} {igual} {c}{tutor.ToUpper()}{c} {cc}" +
            $"{telefono_a} {igual} {c}{telefono.ToUpper()}{c} {cc}" +
            $"{telefono_2_a} {igual} {c}{telefono_2.ToUpper()}{c} {cc}" +
            $"{tipo_sangre_a} {igual} {c}{tipo_sangre.ToUpper()}{c} {cc}" +
            $"{nivel_estudios_a} {igual} {c}SECUNDARIA{c} {cc}" +
            $"{domicilio_a} {igual} {c}{domicilio.ToUpper()}{c} {cc}" +
            //$"{cargo_a} {igual} {c}{cargo.ToUpper()}{c} {cc}" +
            $"{registro_a} {igual} {c}{registro.ToUpper()}{c} {cc}" +
            $"{CURP_a} {igual} {c}{CURP.ToUpper()}{c}";
            //$"{codigo_a} {igual} {c}{Codigo}{c}";
            }

       

        public string valores()
        {
            id_alumno = maxIDAlumno();
            return $"{abrirParentesis}" +
                   $"{c}" +
                   $"{id_alumno}{ccc}"+
                   $"{nombre_alumno.ToUpper().Trim()}{ccc}" +
                   $"{apellido_alumno.ToUpper().Trim()}{ccc}" +
                   $"{grado}{ccc}{grupo.ToUpper()}{ccc}" +
                   //$"{hora_entrada}{ccc}"+
                   //$"{hora_salida}{ccc}"+
                   $"{tutor.ToUpper().Trim()}{ccc}" +
                   $"{telefono}{ccc}" +
                   $"{telefono_2}{ccc}" +
                   $"{tipo_sangre}{ccc}" +
                   $"{nivel_estudios}{ccc}" +
                   $"{domicilio}{ccc}" +
                   //$"{cargo}{ccc}" +
                   $"{registro}{ccc}" +
                   $"{CURP.ToUpper().Trim()}" +
                   //$"{Codigo}" +
                   $"{c}" +
                   $"{cerrarParantesis} ";

        }
        #endregion

        #region METODOS

        /*public void modificarAlumno(string codigo)
        {
            actualizar(queryActualizar(ALUMNO,valoresUpdate(),$"{WHERE}{codigo_a}{igual}{c}{codigo}{c}"));
        }*/
        public void EminarAlumno()
        {

        }
        public bool InsertarAlumno()
        {
           return insertar(queryInsertar(tabla, valores()));
        }

        public bool actualizar()
        {
            return insertar(queryActualizar(ALUMNO, valoresUpdate(), WHERE + id_alumno_a + igual + c + Id_alumno + c));
        }

        public string maxIDAlumno()
        {
            try {
                obtenerDatos = consulta(queryConsulta(maxId(id_alumno_a), tabla, ""), tabla);
                id_alumno = (1 + Int32.Parse(obtenerDatos.Tables[tabla].Rows[0][0].ToString())).ToString();
                return id_alumno;
            }
            catch
            {
                return "1";
            }
        }

        public bool continuidad()
        {
            try
            {
                //obtenerDatos = 
                //select id_entrada_salida from  LIMIT 1
                consulta(queryConsulta(id_entrada_salida_a, ENTRADAS_SALIDAS, $"LIMIT 1"), tabla);
                //String id_alumno_entr =obtenerDatos.Tables[ENTRADAS_SALIDAS].Rows[0][0].ToString();
                Id_alumno = obtenerDatos.Tables[ENTRADAS_SALIDAS].Rows[0][0].ToString();
                return true;
            }

            catch (IndexOutOfRangeException)
            {
                return false;
            }

        }

        public bool cargar(string id_alumno)
        {
            try
            {
                //obtenerDatos = 
                consulta(queryConsulta(ASTERISCO, tabla, $"{WHERE} id_alumnos {igual} {c}{id_alumno}{c}"), tabla);

                Id_alumno = obtenerDatos.Tables[tabla].Rows[0][0].ToString();
                this.Nombre_alumno = obtenerDatos.Tables[tabla].Rows[0][1].ToString();
                this.Apellido_alumno = obtenerDatos.Tables[tabla].Rows[0][2].ToString();
                this.Grado = obtenerDatos.Tables[tabla].Rows[0][3].ToString();
                this.Grupo = obtenerDatos.Tables[tabla].Rows[0][4].ToString();
                //this.hora_entrada = obtenerDatos.Tables[tabla].Rows[0][5].ToString();
                //this.hora_salida = obtenerDatos.Tables[tabla].Rows[0][6].ToString();
                this.Tutor = obtenerDatos.Tables[tabla].Rows[0][5].ToString();
                this.Telefono = obtenerDatos.Tables[tabla].Rows[0][6].ToString();
                this.Telefono_2 = obtenerDatos.Tables[tabla].Rows[0][7].ToString();
                this.Tipo_sangre = obtenerDatos.Tables[tabla].Rows[0][8].ToString();
                this.Nivel_estudios = obtenerDatos.Tables[tabla].Rows[0][9].ToString();
                this.Domicilio = obtenerDatos.Tables[tabla].Rows[0][10].ToString();
                //this.Cargo = obtenerDatos.Tables[tabla].Rows[0][13].ToString();
                this.registro = obtenerDatos.Tables[tabla].Rows[0][11].ToString();
                this.CURP = obtenerDatos.Tables[tabla].Rows[0][12].ToString();
                return true;
            }

            catch(IndexOutOfRangeException)
            {
                return false;
            }
        }

        public void cargarComboBoxAlumno(ComboBox cbNombre, string atributo, ref int entra)
        {
            llenarComboBox(cbNombre, queryConsulta($"{id_alumno_a}", tabla, $"{ORDEN_BY} {atributo} {ASC}"), atributo, tabla, ref entra);
            //llenarComboBox(cbNombre, queryConsulta($"{abrirParentesis}{nombre_alumno_a}||' '||{apellido_alumno_a}{cerrarParantesis} {AS}{nombre_alumno_a}", tabla, ORDEN_BY + nombre_alumno_a + ASC), nombre_alumno_a, tabla);
        }

        
        public void llenarListBox(ListBox listBox,string atributo, string text,ref int entra)
        { 
            llenarListBox(listBox, queryConsulta($"{atributo}", tabla, $"WHERE nombre_alumno LIKE '%{text}%' OR apellido_alumno LIKE '%{text}%' ORDER BY id_alumnos ASC"),$"{atributo}",tabla, ref entra);
            //llenarComboBox(cbNombre, queryConsulta($"{abrirParentesis}{nombre_alumno_a}||' '||{apellido_alumno_a}{cerrarParantesis} {AS}{nombre_alumno_a}", tabla, ORDEN_BY + nombre_alumno_a + ASC), nombre_alumno_a, tabla);
        }

        public void llenarListaIdAlumno(string text)
        {
            lista_id_alumnos.Clear();

            consulta(queryConsulta(id_alumno_a, tabla, $"WHERE nombre_alumno LIKE '%{text}%' OR apellido_alumno LIKE '%{text}%' ORDER BY id_alumnos ASC"), tabla);

            //Id_alumno = obtenerDatos.Tables[tabla].Rows[0][0].ToString();

            for (int i = 0; i < obtenerDatos.Tables[tabla].Rows.Count; i++)
            {
                lista_id_alumnos.Add(obtenerDatos.Tables[tabla].Rows[i][0].ToString());
            }

            //llenarListBox(listBox, queryConsulta($"{atributo}", tabla, $"WHERE nombre_alumno LIKE '%{text}%' OR apellido_alumno LIKE '%{text}%' ORDER BY id_alumnos ASC"), $"{atributo}", tabla, ref entra);
        }



        /* public void llenarIdAlumno()
         {
             try
             {
                 obtenerDatos = consulta(queryConsulta(id_alumno_a, tabla, ORDEN_BY + $"{abrirParentesis}{nombre_alumno_a}||' '||{apellido_alumno_a}{cerrarParantesis}" + ASC), tabla);

                 foreach (System.Data.DataRow id in obtenerDatos.Tables[tabla].Rows)
                 {
                     lista_id_alumno.Add(id[id_alumno_a].ToString());
                 }

             }

             catch (IndexOutOfRangeException)
             {

             }
         }*/

        #endregion

        #region SET AND GET

        public string Id_alumno
        {
            get
            {
                return id_alumno;
            }

            set
            {
                id_alumno = value;
            }
        }

        public string Nombre_alumno
        {
            get
            {
                return nombre_alumno;
            }

            set
            {
                nombre_alumno = value;
            }
        }

        public string Apellido_alumno
        {
            get
            {
                return apellido_alumno;
            }

            set
            {
                apellido_alumno = value;
            }
        }
        public string Grado
        {
            get
            {
                return grado;
            }

            set
            {
                grado = value;
            }
        }

        public string Grupo
        {
            get
            {
                return grupo;
            }

            set
            {
                grupo = value;
            }
        }

        public string Tutor
        {
            get
            {
                return tutor;
            }

            set
            {
                tutor = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Telefono_2
        {
            get
            {
                return telefono_2;
            }

            set
            {
                telefono_2 = value;
            }
        }

        public string Tipo_sangre
        {
            get
            {
                return tipo_sangre;
            }

            set
            {
                tipo_sangre = value;
            }
        }

        public string Nivel_estudios
        {
            get
            {
                return nivel_estudios;
            }

            set
            {
                nivel_estudios = value;
            }
        }

        public string Domicilio
        {
            get
            {
                return domicilio;
            }

            set
            {
                domicilio = value;
            }
        }

        /*public string Cargo
        {
            get
            {
                return cargo;
            }

            set
            {
                cargo = value;
            }
        }
        */
        public string Registro
        {
            get
            {
                return registro;
            }

            set
            {
                registro = value;
            }
        }

        /*public string Ruta_foto
        {
            get
            {
                return ruta_foto;
            }

            set
            {
                ruta_foto = value;
            }
        }

        public string Ruta_frente
        {
            get
            {
                return ruta_frente;
            }

            set
            {
                ruta_frente = value;
            }
        }

        public string Ruta_vuelta
        {
            get
            {
                return ruta_vuelta;
            }

            set
            {
                ruta_vuelta = value;
            }
        }
        */
        public string CURP1
        {
            get
            {
                return CURP;
            }

            set
            {
                CURP = value;
            }
        }

        public string ATRIBUTOS_INSERTAR
        {
            get
            {
                return atributos_insertar;
            }
        }

        /*public string Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }
        */
        /*public string Hora_entrada
        {
            get
            {
                return hora_entrada;
            }

            set
            {
                hora_entrada = value;
            }
        }
        */
        /*public string Hora_salida
        {
            get
            {
                return hora_salida;
            }

            set
            {
                hora_salida = value;
            }
        }*/
        #endregion
    }
}
