﻿namespace PROYECTO
{
    partial class FrmRegistro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button BtnRevisarRegistros;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegistro));
            this.CbNivelEstudio = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.TxtApellido = new System.Windows.Forms.TextBox();
            this.lblnombre = new System.Windows.Forms.Label();
            this.lblapellidos = new System.Windows.Forms.Label();
            this.TxtDomicilio = new System.Windows.Forms.TextBox();
            this.TxtTutor = new System.Windows.Forms.TextBox();
            this.TxtTelefono = new System.Windows.Forms.TextBox();
            this.TxtTelefono2 = new System.Windows.Forms.TextBox();
            this.TxtGrado = new System.Windows.Forms.TextBox();
            this.TxtGrupo = new System.Windows.Forms.TextBox();
            this.TxtRegistro = new System.Windows.Forms.TextBox();
            this.TxtCURP = new System.Windows.Forms.TextBox();
            this.CbTipoSangre = new System.Windows.Forms.ComboBox();
            this.lbldomicilio = new System.Windows.Forms.Label();
            this.lbltutor = new System.Windows.Forms.Label();
            this.lbltelefono = new System.Windows.Forms.Label();
            this.lbltelefono2 = new System.Windows.Forms.Label();
            this.lblgrupo = new System.Windows.Forms.Label();
            this.lblgrado = new System.Windows.Forms.Label();
            this.lblregistro = new System.Windows.Forms.Label();
            this.lbltiposangre = new System.Windows.Forms.Label();
            this.lblcurp = new System.Windows.Forms.Label();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnMonitor = new System.Windows.Forms.Button();
            this.BtnGuardar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbnombre = new System.Windows.Forms.ListBox();
            this.lbapellido = new System.Windows.Forms.ListBox();
            this.lblTurnos = new System.Windows.Forms.Label();
            this.lBturnos = new System.Windows.Forms.ListBox();
            BtnRevisarRegistros = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnRevisarRegistros
            // 
            BtnRevisarRegistros.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnRevisarRegistros.BackgroundImage")));
            BtnRevisarRegistros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            BtnRevisarRegistros.FlatAppearance.BorderSize = 0;
            BtnRevisarRegistros.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            BtnRevisarRegistros.Location = new System.Drawing.Point(540, 15);
            BtnRevisarRegistros.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            BtnRevisarRegistros.Name = "BtnRevisarRegistros";
            BtnRevisarRegistros.Size = new System.Drawing.Size(87, 80);
            BtnRevisarRegistros.TabIndex = 11;
            BtnRevisarRegistros.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            BtnRevisarRegistros.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            BtnRevisarRegistros.UseVisualStyleBackColor = true;
            BtnRevisarRegistros.Click += new System.EventHandler(this.BtnRevisarRegistros_Click);
            // 
            // CbNivelEstudio
            // 
            this.CbNivelEstudio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.CbNivelEstudio.FormattingEnabled = true;
            this.CbNivelEstudio.Items.AddRange(new object[] {
            "PREESCOLAR",
            "PRIMARIA",
            "SECUNDARIA"});
            this.CbNivelEstudio.Location = new System.Drawing.Point(1056, 502);
            this.CbNivelEstudio.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.CbNivelEstudio.Name = "CbNivelEstudio";
            this.CbNivelEstudio.Size = new System.Drawing.Size(201, 37);
            this.CbNivelEstudio.TabIndex = 12;
            this.CbNivelEstudio.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label2.Location = new System.Drawing.Point(1051, 457);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(207, 29);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nivel de Estudios:";
            this.label2.Visible = false;
            // 
            // TxtNombre
            // 
            this.TxtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.TxtNombre.Location = new System.Drawing.Point(35, 188);
            this.TxtNombre.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.Size = new System.Drawing.Size(327, 34);
            this.TxtNombre.TabIndex = 0;
            this.TxtNombre.TextChanged += new System.EventHandler(this.TxtNombre_TextChanged);
            // 
            // TxtApellido
            // 
            this.TxtApellido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtApellido.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.TxtApellido.Location = new System.Drawing.Point(408, 189);
            this.TxtApellido.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.TxtApellido.Name = "TxtApellido";
            this.TxtApellido.Size = new System.Drawing.Size(327, 34);
            this.TxtApellido.TabIndex = 1;
            this.TxtApellido.TextChanged += new System.EventHandler(this.TxtApellido_TextChanged_1);
            // 
            // lblnombre
            // 
            this.lblnombre.AutoSize = true;
            this.lblnombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblnombre.Location = new System.Drawing.Point(29, 155);
            this.lblnombre.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblnombre.Name = "lblnombre";
            this.lblnombre.Size = new System.Drawing.Size(129, 29);
            this.lblnombre.TabIndex = 6;
            this.lblnombre.Text = "Nombre(s)";
            // 
            // lblapellidos
            // 
            this.lblapellidos.AutoSize = true;
            this.lblapellidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblapellidos.Location = new System.Drawing.Point(402, 156);
            this.lblapellidos.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblapellidos.Name = "lblapellidos";
            this.lblapellidos.Size = new System.Drawing.Size(130, 29);
            this.lblapellidos.TabIndex = 7;
            this.lblapellidos.Text = "Apellido(s)";
            // 
            // TxtDomicilio
            // 
            this.TxtDomicilio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtDomicilio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.TxtDomicilio.Location = new System.Drawing.Point(32, 388);
            this.TxtDomicilio.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.TxtDomicilio.Name = "TxtDomicilio";
            this.TxtDomicilio.Size = new System.Drawing.Size(705, 34);
            this.TxtDomicilio.TabIndex = 3;
            this.TxtDomicilio.Enter += new System.EventHandler(this.TxtDomicilio_Enter);
            // 
            // TxtTutor
            // 
            this.TxtTutor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtTutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.TxtTutor.Location = new System.Drawing.Point(34, 276);
            this.TxtTutor.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.TxtTutor.Name = "TxtTutor";
            this.TxtTutor.Size = new System.Drawing.Size(701, 34);
            this.TxtTutor.TabIndex = 2;
            this.TxtTutor.TextChanged += new System.EventHandler(this.TxtTutor_TextChanged);
            this.TxtTutor.Enter += new System.EventHandler(this.TxtTutor_Enter);
            // 
            // TxtTelefono
            // 
            this.TxtTelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.TxtTelefono.Location = new System.Drawing.Point(34, 501);
            this.TxtTelefono.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.TxtTelefono.Name = "TxtTelefono";
            this.TxtTelefono.Size = new System.Drawing.Size(328, 34);
            this.TxtTelefono.TabIndex = 4;
            this.TxtTelefono.TextChanged += new System.EventHandler(this.TxtTelefono_TextChanged);
            this.TxtTelefono.Enter += new System.EventHandler(this.TxtTelefono_Enter);
            // 
            // TxtTelefono2
            // 
            this.TxtTelefono2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.TxtTelefono2.Location = new System.Drawing.Point(1049, 601);
            this.TxtTelefono2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.TxtTelefono2.Name = "TxtTelefono2";
            this.TxtTelefono2.Size = new System.Drawing.Size(215, 34);
            this.TxtTelefono2.TabIndex = 5;
            this.TxtTelefono2.Visible = false;
            // 
            // TxtGrado
            // 
            this.TxtGrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtGrado.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.TxtGrado.Location = new System.Drawing.Point(407, 501);
            this.TxtGrado.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.TxtGrado.Name = "TxtGrado";
            this.TxtGrado.Size = new System.Drawing.Size(84, 34);
            this.TxtGrado.TabIndex = 5;
            this.TxtGrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtGrado.Enter += new System.EventHandler(this.TxtGrado_Enter);
            // 
            // TxtGrupo
            // 
            this.TxtGrupo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtGrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.TxtGrupo.Location = new System.Drawing.Point(523, 501);
            this.TxtGrupo.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.TxtGrupo.Name = "TxtGrupo";
            this.TxtGrupo.Size = new System.Drawing.Size(84, 34);
            this.TxtGrupo.TabIndex = 6;
            this.TxtGrupo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtGrupo.Enter += new System.EventHandler(this.TxtGrupo_Enter);
            // 
            // TxtRegistro
            // 
            this.TxtRegistro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.TxtRegistro.Location = new System.Drawing.Point(1049, 528);
            this.TxtRegistro.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.TxtRegistro.Name = "TxtRegistro";
            this.TxtRegistro.Size = new System.Drawing.Size(213, 34);
            this.TxtRegistro.TabIndex = 10;
            this.TxtRegistro.Visible = false;
            // 
            // TxtCURP
            // 
            this.TxtCURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtCURP.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.TxtCURP.Location = new System.Drawing.Point(1051, 448);
            this.TxtCURP.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.TxtCURP.Name = "TxtCURP";
            this.TxtCURP.Size = new System.Drawing.Size(213, 34);
            this.TxtCURP.TabIndex = 14;
            this.TxtCURP.Visible = false;
            // 
            // CbTipoSangre
            // 
            this.CbTipoSangre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.CbTipoSangre.FormattingEnabled = true;
            this.CbTipoSangre.Items.AddRange(new object[] {
            "AB+",
            "AB-",
            "A+",
            "A-",
            "B+",
            "B-",
            "O+",
            "O-"});
            this.CbTipoSangre.Location = new System.Drawing.Point(1049, 432);
            this.CbTipoSangre.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.CbTipoSangre.Name = "CbTipoSangre";
            this.CbTipoSangre.Size = new System.Drawing.Size(183, 37);
            this.CbTipoSangre.TabIndex = 13;
            this.CbTipoSangre.Visible = false;
            // 
            // lbldomicilio
            // 
            this.lbldomicilio.AutoSize = true;
            this.lbldomicilio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lbldomicilio.Location = new System.Drawing.Point(35, 355);
            this.lbldomicilio.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbldomicilio.Name = "lbldomicilio";
            this.lbldomicilio.Size = new System.Drawing.Size(114, 29);
            this.lbldomicilio.TabIndex = 17;
            this.lbldomicilio.Text = "Domicilio";
            // 
            // lbltutor
            // 
            this.lbltutor.AutoSize = true;
            this.lbltutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lbltutor.Location = new System.Drawing.Point(29, 241);
            this.lbltutor.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbltutor.Name = "lbltutor";
            this.lbltutor.Size = new System.Drawing.Size(70, 29);
            this.lbltutor.TabIndex = 18;
            this.lbltutor.Text = "Tutor";
            // 
            // lbltelefono
            // 
            this.lbltelefono.AutoSize = true;
            this.lbltelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lbltelefono.Location = new System.Drawing.Point(29, 468);
            this.lbltelefono.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbltelefono.Name = "lbltelefono";
            this.lbltelefono.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbltelefono.Size = new System.Drawing.Size(110, 29);
            this.lbltelefono.TabIndex = 19;
            this.lbltelefono.Text = "Telefono";
            // 
            // lbltelefono2
            // 
            this.lbltelefono2.AutoSize = true;
            this.lbltelefono2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lbltelefono2.Location = new System.Drawing.Point(1044, 567);
            this.lbltelefono2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbltelefono2.Name = "lbltelefono2";
            this.lbltelefono2.Size = new System.Drawing.Size(135, 29);
            this.lbltelefono2.TabIndex = 20;
            this.lbltelefono2.Text = "Telefono 2:";
            this.lbltelefono2.Visible = false;
            // 
            // lblgrupo
            // 
            this.lblgrupo.AutoSize = true;
            this.lblgrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblgrupo.Location = new System.Drawing.Point(523, 468);
            this.lblgrupo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblgrupo.Name = "lblgrupo";
            this.lblgrupo.Size = new System.Drawing.Size(80, 29);
            this.lblgrupo.TabIndex = 21;
            this.lblgrupo.Text = "Grupo";
            // 
            // lblgrado
            // 
            this.lblgrado.AutoSize = true;
            this.lblgrado.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblgrado.Location = new System.Drawing.Point(407, 468);
            this.lblgrado.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblgrado.Name = "lblgrado";
            this.lblgrado.Size = new System.Drawing.Size(80, 29);
            this.lblgrado.TabIndex = 22;
            this.lblgrado.Text = "Grado";
            // 
            // lblregistro
            // 
            this.lblregistro.AutoSize = true;
            this.lblregistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblregistro.Location = new System.Drawing.Point(1044, 487);
            this.lblregistro.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblregistro.Name = "lblregistro";
            this.lblregistro.Size = new System.Drawing.Size(110, 29);
            this.lblregistro.TabIndex = 23;
            this.lblregistro.Text = "Registro:";
            this.lblregistro.Visible = false;
            // 
            // lbltiposangre
            // 
            this.lbltiposangre.AutoSize = true;
            this.lbltiposangre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lbltiposangre.Location = new System.Drawing.Point(1044, 386);
            this.lbltiposangre.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbltiposangre.Name = "lbltiposangre";
            this.lbltiposangre.Size = new System.Drawing.Size(187, 29);
            this.lbltiposangre.TabIndex = 24;
            this.lbltiposangre.Text = "Tipo de Sangre:";
            this.lbltiposangre.Visible = false;
            // 
            // lblcurp
            // 
            this.lblcurp.AutoSize = true;
            this.lblcurp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblcurp.Location = new System.Drawing.Point(1045, 406);
            this.lblcurp.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblcurp.Name = "lblcurp";
            this.lblcurp.Size = new System.Drawing.Size(86, 29);
            this.lblcurp.TabIndex = 25;
            this.lblcurp.Text = "CURP:";
            this.lblcurp.Visible = false;
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminar.BackgroundImage")));
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminar.FlatAppearance.BorderSize = 0;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Location = new System.Drawing.Point(1205, 343);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(59, 55);
            this.btnEliminar.TabIndex = 19;
            this.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Visible = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnModificar.BackgroundImage")));
            this.btnModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnModificar.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnModificar.FlatAppearance.BorderSize = 0;
            this.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificar.Location = new System.Drawing.Point(175, 15);
            this.btnModificar.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(60, 55);
            this.btnModificar.TabIndex = 9;
            this.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.BackgroundImage")));
            this.btnLimpiar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLimpiar.FlatAppearance.BorderSize = 0;
            this.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpiar.Location = new System.Drawing.Point(302, 20);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(60, 55);
            this.btnLimpiar.TabIndex = 10;
            this.btnLimpiar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnMonitor
            // 
            this.btnMonitor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMonitor.BackgroundImage")));
            this.btnMonitor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMonitor.FlatAppearance.BorderSize = 0;
            this.btnMonitor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMonitor.Location = new System.Drawing.Point(652, 15);
            this.btnMonitor.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnMonitor.Name = "btnMonitor";
            this.btnMonitor.Size = new System.Drawing.Size(87, 80);
            this.btnMonitor.TabIndex = 12;
            this.btnMonitor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMonitor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMonitor.UseVisualStyleBackColor = true;
            this.btnMonitor.Click += new System.EventHandler(this.btnMonitor_Click);
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.BackColor = System.Drawing.SystemColors.Control;
            this.BtnGuardar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnGuardar.BackgroundImage")));
            this.BtnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnGuardar.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.BtnGuardar.FlatAppearance.BorderSize = 0;
            this.BtnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.BtnGuardar.Location = new System.Drawing.Point(40, 15);
            this.BtnGuardar.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.Size = new System.Drawing.Size(60, 55);
            this.BtnGuardar.TabIndex = 8;
            this.BtnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnGuardar.UseVisualStyleBackColor = false;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 79);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 24);
            this.label1.TabIndex = 26;
            this.label1.Text = "Guardar";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(162, 79);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 24);
            this.label5.TabIndex = 29;
            this.label5.Text = "Actualizar";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(298, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 24);
            this.label3.TabIndex = 31;
            this.label3.Text = "Limpiar";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(533, 101);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 24);
            this.label4.TabIndex = 32;
            this.label4.Text = "Consultas";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(656, 101);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 24);
            this.label7.TabIndex = 33;
            this.label7.Text = "Monitor";
            // 
            // lbnombre
            // 
            this.lbnombre.FormattingEnabled = true;
            this.lbnombre.ItemHeight = 16;
            this.lbnombre.Location = new System.Drawing.Point(35, 230);
            this.lbnombre.Name = "lbnombre";
            this.lbnombre.Size = new System.Drawing.Size(328, 148);
            this.lbnombre.TabIndex = 36;
            this.lbnombre.Visible = false;
            this.lbnombre.SelectedIndexChanged += new System.EventHandler(this.lbnombre_SelectedIndexChanged);
            // 
            // lbapellido
            // 
            this.lbapellido.FormattingEnabled = true;
            this.lbapellido.ItemHeight = 16;
            this.lbapellido.Location = new System.Drawing.Point(408, 230);
            this.lbapellido.Name = "lbapellido";
            this.lbapellido.Size = new System.Drawing.Size(327, 148);
            this.lbapellido.TabIndex = 37;
            this.lbapellido.Visible = false;
            this.lbapellido.SelectedIndexChanged += new System.EventHandler(this.lbapellido_SelectedIndexChanged);
            // 
            // lblTurnos
            // 
            this.lblTurnos.AutoSize = true;
            this.lblTurnos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblTurnos.Location = new System.Drawing.Point(633, 468);
            this.lblTurnos.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTurnos.Name = "lblTurnos";
            this.lblTurnos.Size = new System.Drawing.Size(77, 29);
            this.lblTurnos.TabIndex = 39;
            this.lblTurnos.Text = "Turno";
            this.lblTurnos.Click += new System.EventHandler(this.label6_Click);
            // 
            // lBturnos
            // 
            this.lBturnos.FormattingEnabled = true;
            this.lBturnos.ItemHeight = 16;
            this.lBturnos.Items.AddRange(new object[] {
            "MATUTINO",
            "VESPERTINO"});
            this.lBturnos.Location = new System.Drawing.Point(633, 499);
            this.lBturnos.Name = "lBturnos";
            this.lBturnos.Size = new System.Drawing.Size(102, 36);
            this.lBturnos.TabIndex = 7;
            // 
            // FrmRegistro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 558);
            this.Controls.Add(this.lblTurnos);
            this.Controls.Add(this.lBturnos);
            this.Controls.Add(this.lbapellido);
            this.Controls.Add(this.lbnombre);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMonitor);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(BtnRevisarRegistros);
            this.Controls.Add(this.BtnGuardar);
            this.Controls.Add(this.lblcurp);
            this.Controls.Add(this.lbltiposangre);
            this.Controls.Add(this.lblregistro);
            this.Controls.Add(this.lblgrado);
            this.Controls.Add(this.lblgrupo);
            this.Controls.Add(this.lbltelefono2);
            this.Controls.Add(this.lbltelefono);
            this.Controls.Add(this.lbltutor);
            this.Controls.Add(this.lbldomicilio);
            this.Controls.Add(this.CbTipoSangre);
            this.Controls.Add(this.TxtCURP);
            this.Controls.Add(this.TxtRegistro);
            this.Controls.Add(this.TxtGrupo);
            this.Controls.Add(this.TxtGrado);
            this.Controls.Add(this.TxtTelefono2);
            this.Controls.Add(this.TxtTelefono);
            this.Controls.Add(this.TxtTutor);
            this.Controls.Add(this.TxtDomicilio);
            this.Controls.Add(this.lblapellidos);
            this.Controls.Add(this.lblnombre);
            this.Controls.Add(this.TxtApellido);
            this.Controls.Add(this.TxtNombre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CbNivelEstudio);
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "FrmRegistro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "REALIZE 1.0.0";
            this.Load += new System.EventHandler(this.FrmRegistro_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblnombre;
        private System.Windows.Forms.Label lblapellidos;
        private System.Windows.Forms.Label lbldomicilio;
        private System.Windows.Forms.Label lbltutor;
        private System.Windows.Forms.Label lbltelefono;
        private System.Windows.Forms.Label lbltelefono2;
        private System.Windows.Forms.Label lblgrupo;
        private System.Windows.Forms.Label lblgrado;
        private System.Windows.Forms.Label lblregistro;
        private System.Windows.Forms.Label lbltiposangre;
        private System.Windows.Forms.Label lblcurp;
        public System.Windows.Forms.TextBox TxtNombre;
        public System.Windows.Forms.TextBox TxtApellido;
        public System.Windows.Forms.TextBox TxtDomicilio;
        public System.Windows.Forms.TextBox TxtTutor;
        public System.Windows.Forms.TextBox TxtTelefono;
        public System.Windows.Forms.TextBox TxtTelefono2;
        public System.Windows.Forms.TextBox TxtGrado;
        public System.Windows.Forms.TextBox TxtGrupo;
        public System.Windows.Forms.TextBox TxtRegistro;
        public System.Windows.Forms.TextBox TxtCURP;
        public System.Windows.Forms.ComboBox CbTipoSangre;
        public System.Windows.Forms.ComboBox CbNivelEstudio;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnMonitor;
        private System.Windows.Forms.Button BtnGuardar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox lbnombre;
        private System.Windows.Forms.ListBox lbapellido;
        private System.Windows.Forms.Label lblTurnos;
        public System.Windows.Forms.ListBox lBturnos;
    }
}

